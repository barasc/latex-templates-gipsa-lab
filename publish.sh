#!/bin/bash


echo "Déploiement sur les pages Gricad"
mkdir public
echo "Espace de téléchargement des templates du GIPSA-Lab, Grenoble" > public/index.html

echo "Publication des templates des slides powerpoint"
REPERTOIRE_SLIDES_POWERPOINT="powerpoint-presentation"
mkdir -p public/$REPERTOIRE_SLIDES_POWERPOINT/
cp $REPERTOIRE_SLIDES_POWERPOINT/template-slides-*.pptx public/$REPERTOIRE_SLIDES_POWERPOINT/

echo "Publication des slides latex"
REPERTOIRE_SLIDES_LATEX="latex-presentation"
mkdir -p public/$REPERTOIRE_SLIDES_LATEX/
cp $REPERTOIRE_SLIDES_LATEX/external-communication.tar public/$REPERTOIRE_SLIDES_LATEX/
cp $REPERTOIRE_SLIDES_LATEX/internal-communication.tar public/$REPERTOIRE_SLIDES_LATEX/

echo "Publication des templates du poster powerpoint"
REPERTOIRE_POSTER_POWERPOINT="powerpoint-poster"
mkdir -p public/$REPERTOIRE_POSTER_POWERPOINT/
cp $REPERTOIRE_POSTER_POWERPOINT/*.ppt* public/$REPERTOIRE_POSTER_POWERPOINT

echo "Publication des templates de poster latex"
REPERTOIRE_POSTER_LATEX="latex-poster"
mkdir -p public/$REPERTOIRE_POSTER_LATEX
cp $REPERTOIRE_POSTER_LATEX/poster-latex.tar public/$REPERTOIRE_POSTER_LATEX

echo "Publication des slides de présentation du labo"
REPERTOIRE_SLIDES_PRESENTATION_LABO="labo-presentation"
mkdir -p public/$REPERTOIRE_SLIDES_PRESENTATION_LABO
cp $REPERTOIRE_SLIDES_PRESENTATION_LABO/base_slides_GIPSA_27042023.pptx public/$REPERTOIRE_SLIDES_PRESENTATION_LABO

echo "Publication des affiches du WELCOME DAY"
REPERTOIRE_AFFICHE="powerpoint-affiche"
mkdir -p public/$REPERTOIRE_AFFICHE
cp $REPERTOIRE_AFFICHE/affiche-gipsa-vertical.pptx public/$REPERTOIRE_AFFICHE


# echo "Publication des galeries de photos"
# REPERTOIRE_GALERIE="evenements"
# mkdir -p public/$REPERTOIRE_GALERIE
# cp $REPERTOIRE_GALERIE/*.html public/$REPERTOIRE_GALERIE