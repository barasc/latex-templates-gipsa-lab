# Supports visuels pour les communications du GIPSA

## Codes couleurs du GIPSA-Lab (au 1er septembre 2022)

| Couleur                                 | RGB             | Hexa        |
|:----------------------------------------|:----------------|:------------|
| Bleu-gris                               | (22, 34, 58)    | #16223A     |
| Rouge                                   | (190, 27, 32)   | #BE1B20     |
| Orange                                  | (237, 125, 32)  | #ED7D20     |
| Vert                                    | (50, 168, 85)   | #32A855     |
| Turquoise                               | (34, 174, 226)  | #22AEE2     |
| Bleu-vert-gris                          | (136, 157, 159) | #889D9F     |

