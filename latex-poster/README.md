# LaTeX poster 

This package contains the necessary materials to generate posters in LaTeX (using beamer package) that are compatible with the recommandations of the Communication division of the Gipsa-lab.

## Installation 

Install the package in your working directory. 


## Description 

### LaTeX sources 

The generation of the headings are provided by the commands \maketitle or \maketitleNoLogo, a version without logo.
Title, author and logo can be filled in \title{}, \author{} and  \titlegraphic{}

The minimal contains of your presentations should be like: 

```tex
% === beamerposter === 
\documentclass[]{beamer}
\usepackage[]{beamerposter}
\usepackage{gipsalab-beamerposter}
\title{title}
\author{author}
\titlegraphic{logo}
\begin{document}
\begin{frame}
\maketitle
\end{frame}
\end{document}
```

See the example (`example_beamerposter.tex`) given in the distribution for details. 

### Compilation

Compilation must be done at least two times with pdflatex to ensure things to be placed at the right place. 

```bash
pdflatex example_xyz.tex
pdflatex example_xyz.tex
open example_xyz.pdf
```

For all these packages, use \vfill or \vspace{} for vertical adjustments. 

If you have any problems or suggestions, don't hesitate to contact the communication division at communication@gipsa-lab.grenoble-inp.fr or directly Cleo BARAS et cleo.baras@gipsa-lab.grenoble-inp.fr

Copyright 2016-2020 G. Becq, Gipsa-lab, UMR5216, CNRS, UGA
Copyright 2022-2024 C. Baras, Gipsa-lab, UMR5216, CNRS, UGA
Date 2024.12.13